import  requests
API = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


def get_input():
    weather_location = input("Enter a city or zip code: ")
    is_ok = validate_input(weather_location)
    if is_ok == False:
        while is_ok == False:
            weather_location = input("Enter a city or zip code: ")
            is_ok = validate_input(weather_location)

    return weather_location

def validate_input(weather_location):
    valid = True

    if weather_location.isnumeric():
        #must be 5 digits
        if len(weather_location) != 5:
            print("A zip code must be five digits")
            valid = False
    elif weather_location.isalpha() == False:
        print("Please enter a city - all alpha characters")
        valid = False
    return valid

def ask_for_units():
    units = input("Would you like: \nC - Celsius\nF - Farenheit/Freedom Units ")
    units = units.upper()
    while units != 'C' and units != 'F':
        print("Please choose either C or F")
        units = input("Would you like: \nC - Celsius\nF - Farenheit/Freedom Units ")
    return units

def call_api(parameters):
    #should probably handle afterwards but in a hurry
    try:
        #response = requests.get("http://api.weatherapi.com/v1/forecast.json", params = parameters)
        response = requests.get("http://api.weatherapi.com/v1/current.json", params = parameters)
        response.raise_for_status()
        data = response.json()
        return data 
    except requests.exceptions.HTTPError as errh:
        print("An Http Error occurred:" + repr(errh))
        return "error"
    except requests.exceptions.ConnectionError as errc:
        print("An Error Connecting to the API occurred:" + repr(errc))
        return "error"
    except requests.exceptions.Timeout as errt:
        print("A Timeout Error occurred:" + repr(errt))
        return "error"
    except requests.exceptions.RequestException as err:
        print("An Unknown Error occurred" + repr(err))
        return "error"

def display_current_data (data,units):
    print (f"Current weather in {data['location']['name']}")
    print (f"Condition: {data['current']['condition']['text']}")
    
    if units == 'F':
        print (f"Wind: {data['current']['wind_mph']} mph from the {data['current']['wind_dir']}")
        print (f"Temperature: {data['current']['temp_f']} F")
        print (f"Feels like: {data['current']['feelslike_f']} F")
    else:
        print (f"Wind: {data['current']['wind_kph']} kph from the {data['current']['wind_dir']}")
        print (f"Temperature: {data['current']['temp_c']} C")
        print (f"Feels like: {data['current']['feelslike_c']} C")
    print (f"\nAir Quality")
    print (f"PM 2.5: {data['current']['air_quality']['pm2_5']}")
    print (f"PM 10: {data['current']['air_quality']['pm10']}")

##################################### main logic %#######################################

weather_location = get_input()
parameters = {
    "key": API,
    "q": weather_location,
    "aqi": "yes"
 #   "days": 5
}

data = call_api(parameters)
if data == "error":
    print("Sorry, something went wrong!")
else:
    units = ask_for_units()
    display_current_data(data, units)

   

