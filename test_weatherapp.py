#requires you to run pytest -s and enter input, but it will still check the values below.  
#I should figure out how to get around that, but I am going to look at the other project instead
import pytest
from main import validate_input

@pytest.mark.parametrize("name, expected", [("A123F",False), ("Chicago", True), ("61761", True), ("123456", False)])
def test_validate_input1(name, expected):
    assert validate_input(name) == expected